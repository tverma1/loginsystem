from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
# Create your views here.

def home(request):
   return render(request, 'authentication/index.html')

def signup(request):
    
    if request.method == 'POST':
        user_name = request.POST['username']
        first_name = request.POST['fname']
        last_name = request.POST['lname']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['cpassword']  
        #for register the user in backend after taking the data
        my_user = User.objects.create_user(user_name, email, password)
        my_user.first_name = first_name
        my_user.last_name = last_name
        my_user.save()
        messages.success(request, 'your account has been successfully created.')
        return redirect('signin')
        
    return render(request, 'authentication/signup.html')

def signin(request):
    if request.method == 'POST':
        user_name = request.POST['username']
        password = request.POST['password']
        #authenticate this user
        user = authenticate(user_name = user_name, password = password)
        if user is not None:
            login(request, user)
            first_name = user.first_name
            return render(request, 'authentication/index.html', {'first_name': first_name})
        else:
            #messages.error(request, 'Bad Crdentials')
            return redirect('home')
       
    return render(request, 'authentication/signin.html')

def signout(request):
    logout(request)
    messages.success(request, "logout successfully")
    return redirect('home')